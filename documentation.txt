Table will be used in Project

Comments          Types         
_________         _________
id_comment        INT
id_product        INT
name              Varchar
mail              Varchar
comment           Varchar
date              Varchar
condition         ENUM


Structure of project

commentModule
|
|_ _ _ commentModule.php
|
|_ _ _ classes
|      |
|      |_ _ _ comment.php
|
|_ _ _ controllers
|      |
|      |_ _ _ admin
|             |
|             |_ _ _ AdminCommentController 
|
|_ _ _ view
|      |
|      |_ _ _ temp.
|      |      |
|      |      |_ _ _ hook
|      |      |      |
|      |      |      |_ _ _ front.tpl 
|      |      |   
|      |      |_ _ _ admin
|      |             |
|      |             |_ _ _ admin.tpl
|      |
|      |_ _ _ css
|      |_ _ _ js
|
|_ _ _ logo.png
|
|_ _ _ sql
|      |_ _ _ install.php
|      |_ _ _ uninstall.php
|      
|_ _ _index.php