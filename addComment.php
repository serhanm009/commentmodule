<?php 
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
	exit;
}

class AddComment extends Module 
{
	public function __construct()
	{
		$this->name = 'addcomment';
		$this->author = 'Intern Team';
		$this->version = '1.0.0';
		parent::__construct();
		$this->displayName = $this->l('AddComment');
		$this->description = $this->l('This is a module for Prestashop 1.6.1.22 that users can add comments to the products');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => '1.7.99.99');

	}

	public function install()
	{
		return parent::install() && $this->registerHook('displayRightColumnProduct') && $this->registerHook('header');
	}

	public function uninstall()
	{
		return parent::uninstall();
	}

	public function hookDisplayRightColumnProduct() 
	{
		if(Tools::getIsset('sendComment')) {
			$name = Tools::getValue('addComment_Name');
			$email = Tools::getValue('addComment_Email');
			$comment = Tools::getValue('addComment_Comment');

		}

		Db::getInstance()->executeS("INSERT INTO `ps_comment` (`name`,`email`,`comment`) values('$name', '$email', '$comment')");

		return $this->display(__FILE__, 'views/templates/hook/ui-form.tpl');
		
	}

	public function hookHeader()
	{
		
		$this->context->controller->addCSS(array(
			$this->_path.'views/css/addcomment.css'
		));

		$this->context->controller->addJS(array(
			$this->_path.'views/js/addcomment.js'
		));
	}

}